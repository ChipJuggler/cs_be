const Koa = require('koa');
const cors = require('koa2-cors');

const app = new Koa();
const router = require('koa-router')();
const bodyParser = require('koa-bodyparser');

require('./api/company_search').route({ router, path: '/company-search' });
require('./api/specialities').route({ router, path: '/specialities' });

app.use(bodyParser());
app.use(cors());

app.on('error', (err) => {
  console.error('server error', err);
});

app.use(router.routes());
app.use(router.allowedMethods());

if (process.env.NODE_ENV !== 'test') {
  app.listen(5050);
}

// for tests
module.exports = { app };
