const Chance = require('chance');
const sample = require('lodash/sample');
const specialities = require('./specialities');

const chance = new Chance();

const companies = (num) =>
  [...Array(num)].map(() => ({
    guid: chance.guid(),
    name: chance.company(),
    logo: 'https://picsum.photos/200',
    speciality: sample(specialities),
    city: chance.city(),
  }));

module.exports = companies;
