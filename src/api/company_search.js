const companiesGen = require('../data_generators/companies');

const companies = companiesGen(100);

function route({ router, path }) {
  router.post(path, async (ctx) => {
    ctx.accepts('json');

    const {
      request: {
        body: { name },
      },
    } = ctx;

    const res = name
      ? companies.filter((c) =>
          c.name.toLowerCase().includes(name.toLowerCase()),
        )
      : companies;

    ctx.status = 200;
    ctx.body = JSON.stringify(res);
  });
}

exports.route = route;
