const specialities = require('../data_generators/specialities');

function route({ router, path }) {
  router.get(path, async (ctx) => {
    ctx.accepts('json');

    ctx.status = 200;
    ctx.body = JSON.stringify(specialities);
  });
}

exports.route = route;
