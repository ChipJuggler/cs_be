module.exports = {
  extends: ['airbnb-base', 'prettier', 'plugin:jest/recommended'],
  plugins: ['prettier', 'jest'],
  env: {
    'jest/globals': true,
  },
  parser: 'babel-eslint',
  rules: {
    camelcase: 'off',
    'global-require': 'off',

    // it's temporary, for easier MVP development
    'no-console': 'off',
  },
};
